﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ModuleTrigger : MonoBehaviour
{
    public TerrainModuleInstance terrainModuleInstance;

    [SerializeField]
    private Collider2D col;
    public Collider2D Collider
    {
        get { return col; }
        set { col = value; }
    }


    [SerializeField]
    private VoidEvent onPressed;
    public VoidEvent OnPressed
    {
        get { return onPressed; }
        set { onPressed = value; }
    }

    private void Awake()
    {
        if (col == null)
            col = GetComponent<Collider2D>();
    }

    public void Pressed()
    {
        onPressed?.Invoke();
        terrainModuleInstance.Pressed();
    }



}
