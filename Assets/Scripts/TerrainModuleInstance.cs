﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody2D))]
public class TerrainModuleInstance : MonoBehaviour
{
    private TerrainManager terrainManager;
    public Rigidbody2D rb;

    public Transform elementsRoot;

    #region Events

    private VoidEvent onPressed;
    public VoidEvent OnPressed
    {
        get { return onPressed; }
        set { onPressed = value; }
    }

    private VoidEvent onTransitionOutDone;
    public VoidEvent OnTransitionOutDone
    {
        get { return onTransitionOutDone; }
        set { onTransitionOutDone = value; }
    }

    private VoidEvent onTransitionInDone;
    public VoidEvent OnTransitionInDone
    {
        get { return onTransitionInDone; }
        set { onTransitionInDone = value; }
    }

    #endregion

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void Awake()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
            rb.isKinematic = true;
        }
    }

    public void SetInverted(bool isInverted = true)
    {
        if(isInverted)
            elementsRoot.transform.localScale = new Vector3(-1,1,1);
        else
            elementsRoot.transform.localScale = Vector3.one;
    }

    public void Initialize(TerrainManager terrainManager, bool isInverted)
    {
        this.terrainManager = terrainManager;
        SetInverted(isInverted);
    }

  /*  private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Pressed();
        }
    }*/

    public void Pressed()
    {
        terrainManager.ModulePressed(this);
    }

    [Button]
    public void TransitionIn(CodeAnimatorCurve curve, Vector2 dir, float magnitude)
    {
        dir.Normalize();
        gameObject.SetActive(true);
        Vector2 startPos = (Vector2)transform.position + dir * magnitude;
        transform.position = startPos;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                rb.MovePosition(startPos - Vector2.one * dir * x * magnitude);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curve.Curve,
            ()=> OnTransitionInDone?.Invoke(), curve.Time);
    }

    [Button]
    public void TransitionOut(CodeAnimatorCurve curve, Vector2 dir, float magnitude)
    {
        dir.Normalize();
        Vector2 startPos = transform.position;
        transform.position = startPos;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                rb.MovePosition(startPos + Vector2.one * dir * x * magnitude);
            }, DeltaTimeType.deltaTime, AnimationType.Simple, curve.Curve,
            () => OnTransitionOutDone?.Invoke(), curve.Time);
    }

}
