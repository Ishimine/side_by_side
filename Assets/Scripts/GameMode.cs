﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class GameMode : ScriptableObject
{
    Game_Manager gm;
    TerrainManager tm;



    public bool isPlaying = false;

    private bool initialized;
    public bool Initialized
    {
        get { return initialized; }
        set { initialized = value; }
    }


    [Button]
    public void Initialize(Game_Manager gm, TerrainManager tm)
    {
        if (initialized) return;
        this.gm = gm;
        this.tm = tm;
        initialized = true;
    }

    public void GM_Start()
    {
        PreStart();
        Start();

    }

    public void GM_End()
    {
        End();
    }

    [Button]
    public void PreStart()
    {
        tm.Initialize();
    }

    [Button]
    public void Start()
    {
        isPlaying = true;

    }

    [Button]
    public void Pause()
    {

    }

    [Button]
    public void Resume()
    {

    }

    [Button]
    public void End()
    {
        isPlaying = false;
    }

    public void Quit()
    {
        if(isPlaying)
        {
            End();
        }
        GM_End();
    }

    public void ModulePressed()
    {

    }
}
