﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class CameraAreaFocus : MonoBehaviour
{
    public Game_Manager gm;

    private Camera cam;
    public Camera Camera
    {
        get
        {
            if (cam == null)
                cam = GetComponent<Camera>();
            return cam;
        }
    }

    private Collider2D col;
    public Collider2D Collider
    {
        get
        {
            if (col == null)
            {
                col = GetComponent<Collider2D>();
            }
            return col;
        }
    }

    private void Awake()
    {
        UpdateCamera();
    }

    [Button]
    void UpdateCamera()
    {
        Camera.orthographicSize = Mathf.Max(gm.GameArea.width / cam.aspect , gm.GameArea.height);
        Camera.transform.position = (Vector3)gm.GameArea.position - Vector3.forward * 10;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(gm.GameArea.position, gm.GameArea.size*2);  
    }



}
