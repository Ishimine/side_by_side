﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class TerrainModule : ScriptableObject
{
    [SerializeField,OnValueChanged("UpdateName")]
    private GameObject prefab;
    public GameObject Prefab
    {
        get { return prefab; }
        set { prefab = value; }
    }

    [SerializeField, Min(0)]
    private int difficulty;
    public int Difficulty
    {
        get { return difficulty; }
        set { difficulty = value; }
    }


    private void UpdateName()
    {
#if UNITY_EDITOR
        if (prefab == null) return;
        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(this), prefab.name + "_SO");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }





}
