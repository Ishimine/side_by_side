﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class Game_Manager : ScriptableObject
{

    public GameMode defaultGameMode;
    public TerrainManager terrainManager;

    [SerializeField]
    private Rect gameArea;
    public Rect GameArea
    {
        get { return gameArea; }
    }


    private GameMode currentGameMode;
    public GameMode CurrentGameMode
    {
        get { return currentGameMode; }
        set { currentGameMode = value; }
    }

    [Button]
    public void ModulePressed()
    {
        CurrentGameMode.ModulePressed();
    }

    public void Initiliaze()
    {
        ///Initialize systems elements
    }

    public void Set_ClassicMode()
    {
        SetGameMode(defaultGameMode);
    }

    [Button]
    public void SetGameMode(GameMode gm)
    {
        if(currentGameMode != null)
        {
            currentGameMode.GM_End();
        }
        currentGameMode = gm;
        currentGameMode.Initialize(this, terrainManager);
        currentGameMode.GM_Start();
    }
}
