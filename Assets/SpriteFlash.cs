﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpriteFlash : MonoBehaviour
{   
    [SerializeField]
    private SpriteRenderer render;

    private CodeAnimator codeAnimator = new CodeAnimator();

    private void OnValidate()
    {
        if (render == null)
        {
            render = GetComponent<SpriteRenderer>();
            baseColor = render.color;
        }
    }

    [SerializeField]
    private Color baseColor = Color.clear;
    public Color BaseColor
    {
        get { return baseColor; }
        set { baseColor = value; }
    }

    public void Execute(FlashConfig c)
    {
        Execute(c.color, c.duration, c.curve);
    }

    [Button]
    public void Execute(Color cFlash, float duration, AnimationCurve curve)
    {
        codeAnimator.StartAnimacion(this, x =>
        {
            render.color = Color.Lerp(cFlash, baseColor, x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, curve,duration);
    }

    public void Execute()
    {
        Execute(Color.white, .5f, AnimationCurve.EaseInOut(0,0,1,1));
    }
}

[System.Serializable]
public struct FlashConfig
{
    public AnimationCurve curve;
    public float duration;
    public Color color;
}