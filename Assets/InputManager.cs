﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class InputManager : MonoBehaviour
{

    public static FingerEvent onTapDone;

    public void Awake()
    {
        LeanTouch.OnFingerTap += OnTapDone;
    }


    private void OnTapDone(LeanFinger leanFinger)
    {
        onTapDone?.Invoke(leanFinger);
    }


}

public delegate void FingerEvent(LeanFinger leanFinger);
