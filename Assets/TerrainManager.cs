﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu]
public class TerrainManager : ScriptableObject
{
    public Game_Manager gameManager;
 //   public GameMode gameMode;


    [OnValueChanged("UpdateOrder")]
    public List<TerrainModule> terrainModulePrefabs = new List<TerrainModule>();

    [SerializeField,Range(0,1)]
    private float currentDifficulty;

    public TerrainModuleInstance[] currentModules = new TerrainModuleInstance[2];

    public CodeAnimatorCurve curveIn;
    public CodeAnimatorCurve curveOut;
    public float displacementMagnitude = 8;

    [Button]
    public void Initialize()
    {
        TransitionModule(GetModule(), 0);
        TransitionModule(GetModule(), 1);
    }

    [Button]
    private void TransitionModule(int id)
    {
        TransitionModule(GetModule(), id);
    }

    [Button]
    private void UpdateOrder()
    {
        terrainModulePrefabs = terrainModulePrefabs.OrderBy(x => x.Difficulty).ToList();
    }

    private void TransitionModule(TerrainModule nModule, int id)
    {
        Vector2 dir;
        if (id == 0)
            dir = Vector2.left;
        else
            dir = Vector2.right;

        if(currentModules[id] != null)
        {
            currentModules[id].OnTransitionOutDone += () => SetModule(nModule, id);
            currentModules[id].TransitionOut(curveOut, dir, displacementMagnitude);
        }
        else
        {
            SetModule(nModule, id);
        }
    }

    private void SetModule(TerrainModule nModule, int id)
    {
        Vector2 dir;
        if (id == 0)    dir = Vector2.left;
        else            dir = Vector2.right;

        if(currentModules[id] != null)
        {
            Destroy(currentModules[id].gameObject);
        }

        //Crear posicionar y activar
        currentModules[id] = Instantiate<GameObject>(nModule.Prefab, null).GetComponent<TerrainModuleInstance>();
        currentModules[id].Initialize(this, id == 1);
        currentModules[id].transform.position = dir * gameManager.GameArea.width;
        currentModules[id].TransitionIn(curveIn, dir, displacementMagnitude);
    }

    public TerrainModule GetModule()
    {
        return terrainModulePrefabs[(int)Random.Range(0, terrainModulePrefabs.Count * currentDifficulty)];
    }

    public void SetDifficulty(float nDifficulty)
    {
        currentDifficulty = Mathf.Clamp01(nDifficulty);
    }

    public void ModulePressed(TerrainModuleInstance terrainModule)
    {
        gameManager.ModulePressed();
        if (terrainModule == currentModules[0])
            TransitionModule(GetModule(), 0);
        else
            TransitionModule(GetModule(), 1);
    }
}

public class PoolWithKeys<K, O>
{
    private Dictionary<K, O> elements = new Dictionary<K, O>();
    private Queue<K> queue = new Queue<K>();

    public PoolWithKeys(int maxElements)
    {

    }
}