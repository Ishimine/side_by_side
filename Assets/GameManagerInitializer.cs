﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerInitializer : MonoBehaviour
{

    public Game_Manager gm;

    private void Awake()
    {
        gm.Initiliaze();
        gm.Set_ClassicMode();
    }
}
