﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody2D))]
public class Player_00 : MonoBehaviour
{
    public Vector2 jumpForce = new Vector2(2,5);

    private Rigidbody2D rb;

    private int direction = 1;

    public float velocityX;

    public float smoothTime = 1;
    private float vel;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(Mathf.SmoothDamp(rb.velocity.x , velocityX * direction, ref vel, smoothTime, float.MaxValue, Time.deltaTime), rb.velocity.y);
    }

    private void OnEnable()
    {
        InputManager.onTapDone -= Jump;
        InputManager.onTapDone += Jump;
    }

    private void OnDisable()
    {
        InputManager.onTapDone -= Jump;
    }

    private void Jump(LeanFinger finger)
    {
        //rb.AddForce(jumpForce, ForceMode2D.Impulse);
        rb.velocity = new Vector2(jumpForce.x*direction, jumpForce.y);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collition with " + collision.collider.name);
        ModuleTrigger mt = collision.collider.GetComponent<ModuleTrigger>();
        if (mt != null)
        {
            SwitchDir();
            mt.Pressed();
        }
    }

    [Button]
    private void SwitchDir()
    {
        vel = 0;
        direction = -direction;
        rb.velocity = new Vector2(jumpForce.x * direction, rb.velocity.y);
    }

}
